import React from 'react';
import { Helmet } from 'react-helmet';

import { Constant } from 'src/constant';

export default () => {
  return (
    <>
      <Helmet>
        <html lang="ja" />

        {/* favicon */}
        <link rel="shortcut icon" href="/uploads/favicon.ico" />
        <link rel="apple-touch-icon" href="/uploads/apple-touch-icon.png" />
        <link
          rel="icon"
          type="image/png"
          href="/uploads/android-chrome-256x256.png"
        />
        {/* OGP */}
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content={Constant.title} />
      </Helmet>

      {process.env.NODE_ENV === 'production' && (
        <Helmet>
          {/* アドセンス */}
          <script
            async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          ></script>

          {/* Google Analytics */}
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GOOGLEANALYTICS}`}
          />
          <script>
            {`
                {
                  window.dataLayer = window.dataLayer || [];
                  function gtag(){dataLayer.push(arguments);}
                  gtag('js', new Date());
                  gtag('config', '${process.env.GOOGLEANALYTICS}', {
                    page_path: window.location.pathname + window.location.search,
                  });

                  window.adsbygoogle = window.adsbygoogle || [];
                }
              `}
          </script>
        </Helmet>
      )}
    </>
  );
};
