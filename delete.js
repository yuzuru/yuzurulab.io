const fetch = require('node-fetch');

const main = async () => {
  const res = await fetch(
    `https://api.cloudflare.com/client/v4/zones/${process.env.MY_CF_ZONE_ID}/purge_cache`,
    {
      method: 'delete',
      headers: {
        'X-Auth-Email': process.env.MY_MAIL,
        'X-Auth-Key': process.env.MY_CF_API_KEY,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        purge_everything: true,
      }),
    }
  );

  if (res.status) {
    console.log('パージ成功');
  } else {
    console.log('パージ失敗');
  }
};

main();
